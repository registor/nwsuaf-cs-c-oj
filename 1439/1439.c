/*--------------------------------------------------------------------------------
* Copyright (c) 2019,西北农林科技大学信息学院计算机科学系
* All rights reserved.
*
* 文件名称：main.c
* 文件标识：见配置管理计划书
* 摘要：nwafu-oj,1439题判断是否为对称矩阵题解。
*
* 思路：
*      1、输入矩阵A
*      2、求解矩阵A的转置矩阵B
*      3、如果A和B相等，则A为对称矩阵，否则不是
*
* 测试数据：
*          4
*          1 2 3 4
*          2 5 6 7
*          3 6 8 9
*          4 7 9 0
*
* 当前版本：1.0
* 作者：耿楠
* 完成日期：2019年12月17日
*
* 取代版本：无
* 原作者：
* 完成日期：
--------------------------------------------------------------------------------*/
#include<stdio.h>
#include<stdlib.h>
#include<string.h>

/* 宏定义 */
#define SIZE 4

/* 函数原型 */
/* 输入矩阵 */
void inputMat(int (*)[SIZE], int, int);
/* 输出矩阵 */
void outputMat(int (*)[SIZE], int, int);
/* 矩阵转置 */
void getTransposeMat(int (*)[SIZE], const int (*)[SIZE], int, int);
/* 对称矩阵判断 */
int isSymmetricMat(const int (*)[SIZE], const int (*)[SIZE], int, int);
/* 利用memcmp判断对称矩阵 */
int isSymmetricMatMem(const void *, const void *, size_t size);
/* 利用异或判断对称矩阵 */
int isSymmetricMatXor(const void *, const void *, size_t size);


/* 测试 */
int main()
{
    int m;
    int a[SIZE][SIZE] = {{1, 2, 3, 4},
                         {2, 5, 6, 7},
                         {3, 6, 8, 9},
                         {4, 7, 9, 0},
                        };
    int b[SIZE][SIZE] = {0};

    scanf("%d", &m);

    inputMat(a, m, m);
    getTransposeMat(b, a, m, m);
    if(isSymmetricMatMem(a, b, m * m * sizeof(int)))
    {
        printf("Yes\n");
    }
    else
    {
        printf("No\n");
    }

    return 0;
}

/* 函数定义
//-----------------------------------------------------------------------------------------------
// 名称: void inputMat(int (*a)[SIZE], int m, int n)
// 功能: 输入二维数组
// 算法:
// 参数:
//       [int (*a)[SIZE]] --- 数组指针
//       [int m] --- 数组行数
//       [int n] --- 数组列数
// 返回: [void]  --- 无
// 作者: 耿楠
// 日期: 2019年12月17日
//---------------------------------------------------------------------------------------------*/
void inputMat(int (*a)[SIZE], int m, int n)
{
    int i, j;

    for(i = 0; i < m; i++)
    {
        for(j = 0; j < n; j++)
        {
            scanf("%d", &a[i][j]);
        }
    }
}

/*-----------------------------------------------------------------------------------------------
// 名称: void outputMat(int (*a)[SIZE], int m, int n)
// 功能: 输入二维数组
// 算法:
// 参数:
//       [int (*a)[SIZE]] --- 数组指针
//       [int m] --- 数组行数
//       [int n] --- 数组列数
// 返回: [void]  --- 无
// 作者: 耿楠
// 日期: 2019年12月17日
//---------------------------------------------------------------------------------------------*/
void outputMat(int (*a)[SIZE], int m, int n)
{
    int i, j;

    for(i = 0; i < m; i++)
    {
        for(j = 0; j < n - 1; j++)
        {
            printf("%d ", a[i][j]);
        }
        printf("%d\n", a[i][j]);
    }
}

/*-----------------------------------------------------------------------------------------------
// 名称: void getTransposeMat(int (*b)[SIZE], const int (*a)[SIZE], int m, int n)
// 功能: 输入二维数组
// 算法: 逐个元素交换实现转置
// 参数:
//       [int (*b)[SIZE]] --- 目标矩阵数组指针
//       [const int (*a)[SIZE]] --- 源矩阵数组指针
//       [int m] --- 数组行数
//       [int n] --- 数组列数
// 返回: [void]  --- 无
// 作者: 耿楠
// 日期: 2019年12月17日
//---------------------------------------------------------------------------------------------*/
void getTransposeMat(int (*b)[SIZE], const int (*a)[SIZE], int m, int n)
{
    int i, j;

    for(i = 0; i < m; i++)
    {
        for(j = 0; j < n; j++)
        {
            b[i][j] = a[j][i];/* 转置 */
        }
    }
}

/*-----------------------------------------------------------------------------------------------
// 名称: int isSymmetricMat(const int (*a)[SIZE], const int (*b)[SIZE], int m, int n)
// 功能: 判断是否为对称矩阵
// 算法: 通过判断一个矩阵是否与其转置矩阵相等进行判断
// 参数:
//       [const int (*a)[SIZE]] --- a数组指针
//       [const int (*b)[SIZE]] --- a的转置矩阵b数组指针
//       [int m] --- 数组行数
//       [int n] --- 数组列数
// 返回: [int]  --- 1，是；0, 否
// 作者: 耿楠
// 日期: 2019年12月17日
//---------------------------------------------------------------------------------------------*/
int isSymmetricMat(const int (*a)[SIZE], const int (*b)[SIZE], int m, int n)
{
    int i, j;

    /* 行列数不相等，不可能是对称阵 */
    if(m != n)
    {
        return 0;
    }

    for(i = 0; i < m; i++)
    {
        for(j = 0; j < n; j++)
        {
            if(a[i][j] != b[i][j])
            {
                return 0; /* 有不相等，则不是对称阵 */
            }
        }
    }

    /* 是对称阵 */
    return 1;
}

/*-----------------------------------------------------------------------------------------------
// 名称: int isSymmetricMatMem(const void *a, const void *b, size_t size)
// 功能: 判断是否为对称矩阵
// 算法: 通过memcmp判断两个数据块是不是相等
// 参数:
//       [const void *a] --- 数据块a的指针
//       [const void *b] --- 数据块b的指针
//       [size_t size] --- 数据块大小
// 返回: [int]  --- 1，是；0, 否
// 作者: 耿楠
// 日期: 2019年12月17日
//---------------------------------------------------------------------------------------------*/
int isSymmetricMatMem(const void *a, const void *b, size_t size)
{
    if(memcmp(a, b, size))
    {
        return 0;
    }
    else
    {
        return 1;
    }
}

/*-----------------------------------------------------------------------------------------------
// 名称: int isSymmetricMatXor(const void *a, const void *b, size_t size)
// 功能: 判断是否为对称矩阵
// 算法: 通过异或运算判断两个数据块是不是相等
// 参数:
//       [const void *a] --- 数据块a的指针
//       [const void *b] --- 数据块b的指针
//       [size_t size] --- 数据块大小
// 返回: [int]  --- 1，是；0, 否
// 作者: 耿楠
// 日期: 2019年12月17日
//---------------------------------------------------------------------------------------------*/
int isSymmetricMatXor(const void *a, const void *b, size_t size)
{
    const unsigned char *pa = (const unsigned char *)a;
    const unsigned char *pb = (const unsigned char *)b;
    int i;

    for(i = 0; i < size; i++)
    {
        if(*pa ^ *pb)
        {
            return 0;
        }
        pa++;
        pb++;
    }

    return 1;
}





