#include <stdio.h>
#include <stdlib.h>

#define N 8

void getCharBin(int [], int , char );
void putCharBin(int [], int );

int main()
{
    /* 用一个有8个元素的整型数组记录各二进制位 */
    int a[N] = {0};

    char ch;

    /* 读入一个字符 */
    scanf("%c", &ch);

    /* 计算 */
    getCharBin(a, N, ch);

    /* 输出 */
    putCharBin(a, N);

    return 0;
}

/* 计算二进数 */
void getCharBin(int a[], int n, char ch)
{
    int i = 0; /* 注意初始化为0 */

    /* 采用%2得到位值，采用/2得到位的位置 */
    /* 注意是a数组中是从低位向高位记录的。 */
    while(ch != 0)
    {
        a[i++] = ch % 2;
        ch /= 2;
    }
}

/* 输出二进制数 */
void putCharBin(int a[], int n)
{
    int i;

    /* 注意是a数组中是从低位向高位记录的。 */
    /* 输出时需要从高位开始输出 */
    for(i = 7; i >= 0; i--)
    {
        printf("%d", a[i]);
    }
}

