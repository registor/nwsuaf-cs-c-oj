#include <stdio.h>
#include <stdlib.h>

void ex_num(int *a, int *b)
{
    *a ^= *b;
    *b ^= *a;
    *a ^= *b;
}

void exchange(int a[], int n)
{
    int left, right;
    int cnt;

    cnt = 1;
    left = 0;
    right = n - 1;
    while (left < right)
    {
        if (!(a[left] & 1) && (a[right] & 1))
        {
            printf("Times %d: %d<==>%d\n", cnt++, a[left], a[right]);
            ex_num(&a[left], &a[right]);
        }
        left = (a[left] & 1) ? left + 1 : left;
        right = (!(a[right] & 1 )) ? right - 1 : right;
    }
    return;
}

int main()
{
    int i = 0;
    int  *arr, sz;

    while(scanf("%d", &sz) != 1);
    arr = (int *)malloc(sz * sizeof(int));
    if (arr == NULL)
        return 0;

    for (i = 0; i < sz; i++)
    {
        while(scanf("%d", arr + i) != 1);
    }

    exchange(arr, sz);
    for (i = 0; i < sz - 1; i++)
    {
        printf("%d ", arr[i]);
    }
    printf("%d\n", arr[i]);
    free(arr);
    return 0;
}

