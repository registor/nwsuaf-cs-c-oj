## 编程题 1500 数组中奇偶数位置调整
### 题目描述
调整正整型数组使奇数全部都位于偶数前面，注意数组的遍历顺序要求为一方从左端开始向后，另一方从右端开始向前，若前数为偶数且后数为奇数，则进行交换，否则继续遍历。程序主体已经完成，但其中包含若干错误，请根据题目要求改正程序中的所有错误，使其顺利达成题目功能要求。
### 输入
正整型数组的元素个数sz(sz<4000)，然后依次为数组各元素的整型值
### 输出
交换过程，其格式为：Times 交换次数：数1<==>数2，每次交换占一行
然后输出交换后的数组各元素，元素之间用一个空格分隔，最后一个元素后无空格
### 样例输入
```c
9
1 2 3 4 5 6 7 8 9
```
### 样例输出
```
Times 1: 2<==>9
Times 2: 4<==>7
1 9 3 7 5 6 4 8 2
```
### *提示*
该题目为改错题，不可以更改程序结构、变量及函数命名，只能根据题目要求，发现程序中的错误，并进行改正。

### 程序主体(错误代码)
```c
#include <stdio.h>
#include <stdlib.h>

/* 交换指针a和b所指向的两个整数 */  
void ex_num(int *a, int *b)
{
    a ^= b;
    b ^= a;
    a ^= b;
}

/* 调整正整型数组a，其中n为数组元素的个数 */
void exchange(int a[], int n)
{
    int left, right;
    int cnt;

    left = 0;
    right = n;
    while (left < right)
    {
        if (!(a[left] & 1) && (a[right] & 1)) {
             printf("Times %d: %d<==>%d\n", cnt++, a[left], a[right]);
             ex_num(&a[left], &a[right]);
        }
        left = (a[left] & 1) ? left+1 : left;
        right = !(a[right] & 1) ? right-1 : right;
    }
    return;
}

int main()
{
    int  *arr, sz;

    while(scanf("%d", sz) != 1);
    arr=(int *)malloc(sz * sizeof(int));
    if (arr==NULL) return 0;
    for (i = 0; i < sz; i++)  {
        while(scanf("%d", arr[i]) != 1);
    }
    exchange(arr, sz);
    for (i = 0; i < sz; i++) {
        printf("%d ", arr+i);
    }
    printf("\b\n");
    free(arr);
    return 0;
}
```
## 参考答案
利用函数设计程序，参见 `1500.c`。

