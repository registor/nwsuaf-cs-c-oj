#include <stdio.h>

/* 计算并输出高精度浮点数计算结果 */
void putFloat(int, int);

int main()
{
    int a, b;

    scanf("%d/%d", &a, &b);

    putFloat(a, b);

    return 0;
}

/* 计算并输出高精度浮点数计算结果 */
/* 注意题目并未要求保留计算结果 */
/* 如果需要保留结果，则应该使用数组实现 */
void putFloat(int a, int b)
{
    int i;
    int m, n;

    printf("0.");

    m = a % b * 10;
    for(i = 0; i < 200; i++)
    {
        n = m / b;
        m = m % b * 10;
        printf("%d", n);

        if(m == 0)
        {
            break;
        }
    }

    printf("\n");
}


