#include<stdio.h>

/* 检查大括号匹配 */
int checkBrace();
void outResult(int);

int main()
{
    int cnt;

    cnt = checkBrace();
    outResult(cnt);

    return 0;
}

/* 检查大括号匹配 */
int checkBrace()
{
    /* 必须为int，否则无法接收EOF */
    int ch = 0;
    int count = 0;

    while((ch = getchar()) != EOF)
    {
        if(ch == '{')
        {
            count++;
        }
        else if(ch == '}' && count == 0)
        {
            return -1;
        }
        else if(ch == '}' && count > 0)
        {
            count--;
        }
    }

    return count;
}

void outResult(int cnt)
{
    if(cnt == -1)
    {
        printf("No(-1)\n");
        return;
    }

    if(cnt == 0)
    {
        printf("Yes\n");
    }
    else
    {
        printf("No(%d)\n", cnt);
    }
}

