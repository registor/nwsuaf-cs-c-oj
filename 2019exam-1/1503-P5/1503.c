#include<stdio.h>
#include<stdlib.h>

// 函数原型
int getLowestTerms(int, int, int *, int *); // 分数简化
int getGCD(int, int); // 计算最大公约数
void Output(int, int, int);

// 测试
int main()
{
    int m, n, Num, Denom;
    int status;

    scanf("%d%d", &m, &n);

    status = getLowestTerms(m, n, &Num, &Denom);
    Output(status, Num, Denom);

    return 0;
}

// 分数化简
// 原分子分母通过参数输入
// 化简后的分子分母写入指针形参指向的变量
int getLowestTerms(int m, int n, int *pNum, int *pDenom)
{
    int gcd;
    int msign = 1;
    int nsign = 1;
    int numsign = 1;

    if(m == 0)
    {
        return 0;
    }
    else if(m == n)
    {
        return 1;
    }
    else if(n == 0)
    {
        return -1;
    }
    else
    {
        // 结果符号判断
        if(m < 0)
        {
            msign = -1;
        }
        if(n < 0)
        {
            nsign = -1;
        }
        // 结果分子符号
        numsign = msign * nsign;

        m = abs(m);
        n = abs(n);
        *pNum = m;
        *pDenom = n;
        gcd = getGCD(m, n);
        *pNum /= gcd;
        *pNum *= numsign;
        *pDenom /= gcd;

        return 2;
    }
}

// 计算最大公约数
int getGCD(int m, int n)
{
    int rem = n;

    while (rem != 0)
    {
        rem = m % n;
        m = n;
        n = rem;
    }

    return m;
}

void Output(int status, int nNum, int nDenom)
{
    if(status == 0)
    {
        printf("%d\n", status);
    }
    else if(status == 1)
    {
        printf("%d\n", status);
    }
    else if (status == 2)
    {
        if(nDenom == 1) // 分母为1
        {
            printf("%d\n", nNum);
        }
        else
        {
            printf("%d/%d\n", nNum, nDenom);
        }
    }
    else
    {
        printf("Error!\n");
    }
}

