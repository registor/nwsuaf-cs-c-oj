#include <stdio.h>
#include <stdlib.h>

int judge(int [], int, int *);

int main()
{
    int *a;
    int n, i, cd;

    scanf("%d", &n);
    a = (int *)malloc(n * sizeof(int));
    for(i = 0; i < n; i++)
    {
        scanf("%d", &a[i]);
    }

    if(!judge(a, n, &cd))
    {
        printf("YES(%d)\n", cd);
    }
    else
    {
        printf("NO\n");
    }

    free(a);

    return 0;
}

