#include <stdio.h>
#include <stdlib.h>

int judge(int [], int, int *);

int main()
{
    int *a;
    int n, i, cd;

    scanf("%d", &n);
    a = (int *)malloc(n * sizeof(int));
    for(i = 0; i < n; i++)
    {
        scanf("%d", &a[i]);
    }

    if(!judge(a, n, &cd))
    {
        printf("YES(%d)\n", cd);
    }
    else
    {
        printf("NO\n");
    }

    free(a);

    return 0;
}

int cmp(const void *a, const void *b)
{
    return (*(int *)a - * (int *)b);
}

int judge(int a[], int n, int *cd)
{
    int i;

    if (n < 3)
    {
        return -1;
    }

    qsort(a, n, sizeof(int), cmp);

    for(i = 1; i < n - 1; i++)
    {
        if((a[i + 1] - a[i]) != (a[i] - a[i - 1]))
        {
            break;
        }
    }

    if (i < n - 1)
    {
        return 1;
    }

    *cd = a[1] - a[0];

    return 0;
}
