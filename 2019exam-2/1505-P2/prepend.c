#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int isprime(int);
int resolve(int);

int main()
{
    int n, m, i, r;

    scanf("%d%d", &m, &n);

    if(m % 2 != 0)
    {
        m++;
    }

    for(i = m; i <= n; i += 2)
    {
        if ((r = resolve(i)) >= 2)
        {
            printf("%d=%d+%d\n", i, r, i - r);
        }
        else
        {
            printf("Cannot resolve %d.\n", i);
        }
    }

    return 0;
}

