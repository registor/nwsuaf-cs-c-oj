#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int isprime(int);
int resolve(int);

int main()
{
    int n, m, i, r;

    scanf("%d%d", &m, &n);

    if(m % 2 != 0)
    {
        m++;
    }

    for(i = m; i <= n; i += 2)
    {
        if ((r = resolve(i)) >= 2)
        {
            printf("%d=%d+%d\n", i, r, i - r);
        }
        else
        {
            printf("Cannot resolve %d.\n", i);
        }
    }

    return 0;
}

int isprime(int x)
{
    int m;

    double r = sqrt(x);

    for(m = 2; m <= r; m++)
    {
        if (x % m == 0)
        {
            return 0;
        }
    }

    return 1;
}

int resolve(int l)
{
    int i;

    for(i = 2; i <= l / 2; i++)
    {
        if((isprime(i)) && (isprime(l - i)))
        {
            return i;
        }
    }

    return 0;
}
