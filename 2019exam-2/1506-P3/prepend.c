#include <stdio.h>
#include <stdlib.h>
#include <math.h>

double f1(double);
double f2(double);
double f3(double);

double BiRoot(double (*fun)(double), double a, double b);

int main()
{
    double a, b;
    int n;

    scanf("%lf%lf%d", &a, &b, &n);

    switch(n)
    {
    case 1:
        printf("%.3f\n", BiRoot(f1, a, b));
        break;
    case 2:
        printf("%.3f\n", BiRoot(f2, a, b));
        break;
    case 3:
        printf("%.3f\n", BiRoot(f3, a, b));
        break;
    default:
        printf("error\n");
        exit(0);
    }

    return 0;
}

double f1(double x)
{
    return x * x * x - 10 * x * x + 3 * x + 20.0;
}

double f2(double x)
{
    return x * x * x - 6 * x - 1;
}

double f3(double x)
{
    return 3 * sin(x) - sin(x) * sin(x) * sin(x);
}
