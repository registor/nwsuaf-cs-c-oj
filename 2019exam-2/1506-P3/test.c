#include <stdio.h>
#include <stdlib.h>
#include <math.h>

double f1(double);
double f2(double);
double f3(double);

double BiRoot(double (*fun)(double), double a, double b);

int main()
{
    double a, b;
    int n;

    scanf("%lf%lf%d", &a, &b, &n);

    switch(n)
    {
    case 1:
        printf("%.3f\n", BiRoot(f1, a, b));
        break;
    case 2:
        printf("%.3f\n", BiRoot(f2, a, b));
        break;
    case 3:
        printf("%.3f\n", BiRoot(f3, a, b));
        break;
    default:
        printf("error\n");
        exit(0);
    }

    return 0;
}

double f1(double x)
{
    return x * x * x - 10 * x * x + 3 * x + 20.0;
}

double f2(double x)
{
    return x * x * x - 6 * x - 1;
}

double f3(double x)
{
    return 3 * sin(x) - sin(x) * sin(x) * sin(x);
}

/* 以上为 prepend.c 内容*/
double BiRoot(double (*fun)(double), double a, double b)
{
    double x;
    if(fun(a)*fun(b) > 0)
    {
        printf("error! a,b have the same sign.\n");
        exit(-1);
    }

    x = (a + b) / 2.0;

    if((fabs(fun(x)) < 1e-6))
    {
        return x;
    }
    else
    {
        if(fun(x)*fun(a) > 0)
        {
            return BiRoot(fun, x, b);
        }
        else
        {
            return BiRoot(fun, a, x);
        }
    }
}
