#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int CountLine(FILE *fp, int n);

int main()
{
    FILE *fp = NULL;
    int a, b, i;

    fp = fopen("test.dic", "r");

    if (fp == NULL)
    {
        puts("Cannot Open Input File.\n");
        exit(0);
    }

    /*scanf("%d%d", &a, &b);*/

    a = 9;

    b = 10;

    for(i = a; i <= b; i++)
    {
        printf("%d\n", CountLine(fp, i));
    }

    fclose(fp);

    return 0;
}
