## 编程题 1508: 阶段2考试题目5 变位词统计
### 题目描述
变位词指的是相同字母的重新排列，如单词"for"的变位词可以是："for"、"ofr"、"rof"、"fOr"、"FRO"等。
给定一串文本字符串和一个单词(字符串和单词长度均不超过255)，现要统计在该字符串中，该单词的变位词出现次数。请完成以下指定函数：
```c
int countAnagrams(const char *text, const char *word); /* 返回文本串text中包含的子串word及其变位词的个数 */
/* 辅助函数 */
int areAnagrams(const char *word1, const char *word2); /* 判断word1与word2是否互为变位词，是返回1，否则返回0 */
void lowcase(char *s); /* 将字符串s中全部字母变为小写 */ 
```
*注意*：

1、变位词判断中不区分大小写。
2、原字符串及单词不能发生改变。
3、程序main()函数及主体构架如下，仅供参考，但不需要提交。

```c
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define STR_LEN 255

int countAnagrams(const char *, const char *); /* 统计变位词个数 */
int areAnagrams(const char *, const char *); /* 变位词判断 */

/* 辅助函数 */
int read_line(char *, int); /* 读入不超过指定长度的字符串 */
void lowcase(char *); /* 将字符串变为小写 */

int main()
{
    char text[STR_LEN + 1] = {'\0'};
    char word[STR_LEN + 1] = {'\0'};
    int cnt = 0;

    read_line(text, STR_LEN);
    read_line(word, STR_LEN);

    cnt = countAnagrams(text, word);

    printf("The text is \"%s\".\n", text);
    printf("The word is \"%s\".\n", word);
    printf("The count of anagrams is %d.\n", cnt);

    return 0;
}

/* 函数实现 */
/* 读入一行不超过指定长度的字符串 */
int read_line(char *str, int n)
{
    int ch, i = 0;

    /* 读入字符，直到遇到换行符 */
    while ((ch = getchar()) != '\n' && ch != '\r' && ch!=EOF)
    {
        if(i < n) /* 最大字符数 */ 
        { str[i++] = ch; }
     }
     str[i] = '\0'; /* 添加空字符'\0' */ 
     return i; /* 返回读入的字符数 */ 
}
```
### 输入
一串文本字符串和一个单词
### 输出
字符串中出现的单词的变位词个数信息，若无变位词，则输出0个
### 样例输入
forxxorfxdofr
for
### 样例输出
The text is "forxxorfxdofr".
The word is "for".
The count of anagrams is 3.
### *提示*
① 可以从字符串中逐个提取与单词长度相等的子字符串，然后与单词进行变位词判断，根据判断结果进行计数处理。

② 变位词的判断可参考教材P127第16题或P224第14题。

③ 请严格按照题目要求编程，实现全部**3个**函数。

## 参考答案
参见 `test.c`。

Linux下，请在命令行执行`make`命令编译`test.c`，编译链接后执行`./test`进行测试。

*`prepend.c`预写代码，并根据需要对指定函数进行调用测试。*
