#include<stdio.h>
#include<stdlib.h>
#include<string.h>

#define STR_LEN 255

int countAnagrams(const char *, const char *); /* 统计变位词个数 */
int areAnagrams(const char *, const char *); /* 变位词判断 */

/* 辅助函数 */
int read_line(char *, int); /* 读入不超过指定长度的字符串 */
void lowcase(char *); /* 将字符串变为小写 */

int main()
{
     char text[STR_LEN + 1] = {'\0'};
     char word[STR_LEN + 1] = {'\0'};
     char teststr[]="PasSed.";
     int cnt = 0;
     
     read_line(text, STR_LEN);
     read_line(word, STR_LEN);
     cnt = countAnagrams(text, word);
     lowcase(teststr);
     printf("Test lowcase():%s\n", teststr);
     printf("Test areAnagrams():%s\n", areAnagrams("abc","bca")?"passed":"Not pass");
     printf("The text is \"%s\".\n", text);
     printf("The word is \"%s\".\n", word);
     printf("The count of anagrams is %d.\n", cnt);
     return 0;
}

/* 函数实现 */
/* 读入一行不超过指定长度的字符串 */
int read_line(char *str, int n)
{
     int ch, i = 0;
     
     /* 读入字符，直到遇到换行符 */
     while ((ch = getchar()) != '\n' && ch != '\r' && ch != EOF)
     {
          if(i < n) /* 最大字符数 */
          {
               str[i++] = ch;
          }
     }
     str[i] = '\0'; /* 添加空字符'\0' */
     return i;    /* 返回读入的字符数 */
}

