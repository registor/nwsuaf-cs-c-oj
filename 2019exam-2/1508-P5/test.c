#include<stdio.h>
#include<stdlib.h>
#include<string.h>

#define STR_LEN 255

int countAnagrams(const char *, const char *); /* 统计变位词个数 */
int areAnagrams(const char *, const char *); /* 变位词判断 */

/* 辅助函数 */
int read_line(char *, int); /* 读入不超过指定长度的字符串 */
void lowcase(char *); /* 将字符串变为小写 */

int main()
{
     char text[STR_LEN + 1] = {'\0'};
     char word[STR_LEN + 1] = {'\0'};
     char teststr[]="PasSed.";
     int cnt = 0;

     read_line(text, STR_LEN);
     read_line(word, STR_LEN);
     cnt = countAnagrams(text, word);
     lowcase(teststr);
     printf("Test lowcase():%s\n", teststr);
     printf("Test areAnagrams():%s\n", areAnagrams("abc","bca")?"passed":"Not pass");
     printf("The text is \"%s\".\n", text);
     printf("The word is \"%s\".\n", word);
     printf("The count of anagrams is %d.\n", cnt);
     return 0;
}

/* 函数实现 */
/* 读入一行不超过指定长度的字符串 */
int read_line(char *str, int n)
{
     int ch, i = 0;

     /* 读入字符，直到遇到换行符 */
     while ((ch = getchar()) != '\n' && ch != '\r' && ch != EOF)
     {
          if(i < n) /* 最大字符数 */
          {
               str[i++] = ch;
          }
     }

     str[i] = '\0'; /* 添加空字符'\0' */

     return i;    /* 返回读入的字符数 */
}

/* 以上为 prepend.c 内容*/
void lowcase(char *s)
{
    while(*s)
    {
        if(*s >= 'A' && *s <= 'Z' )
        {
            *s |= 0x20;
        }
        s++;
    }
}

/* 统计变位词个数 */
int countAnagrams(const char *text, const char *word)
{
    int i;
    int cnt = 0;
    char *s;
    const char *st = text;

    /* 字符串长度 */
    int text_len = strlen(text);
    int word_len = strlen(word);

    /* 在复制的字符串中进行操作 */
    s = (char *)malloc((word_len + 1) * sizeof(char));
    if(NULL == s)
    {
        printf("Not enough memory!\n");
        exit(1);
    }

    for(i = 0; i < text_len - word_len + 1; i++)
    {
        strncpy(s, st + i, word_len);
        s[word_len]='\0';
        if(areAnagrams(s, word))
        {
            cnt++;
            i += word_len - 1;
        }
    }

    free(s);
    return cnt;
}

/* 判断两个单词是不是变位词 */
int areAnagrams(const char *word1, const char *word2)
{
    int i, len1, len2;
    unsigned int letterHist[26] = {0};
    char *s1, *s2, *p1, *p2;

    len1 = strlen(word1);
    len2 = strlen(word2);

    /* 如果长度不相等，则一定不是变位词 */
    if(len1 != len2)
    {
        return 0;
    }

    /* 在复制的字符串中进行操作 */
    p1 = s1 = (char *)malloc((len1 + 1) * sizeof(char));
    p2 = s2 = (char *)malloc((len1 + 1) * sizeof(char));
    if(NULL == s1 || NULL == s2)
    {
        printf("Not enough memory!\n");
        exit(1);
    }

    strcpy(s1, word1);
    strcpy(s2, word2);

    /* 将字符串变为小写 */
    lowcase(s1);
    lowcase(s2);

    /* 统计s1中各个字符出现的次数(加) */
    while(*s1)
    {
        letterHist[*s1 - 'a']++;
        s1++;
    }
    /* 统计s2中各个字符出现的次数(减) */
    while(*s2)
    {
        letterHist[*s2 - 'a']--;
        s2++;
    }

    free(p1);
    free(p2);

    /* 如果有一个不为0，则不是变位词，直接返回 */
    for(i = 0; i < 26; i++)
    {
        if(letterHist[i] != 0)
        {
            return 0;
        }
    }

    return 1;
}

